json.extract! map, :id, :latitude, :longitude, :address, :created_at, :updated_at
json.url map_url(map, format: :json)